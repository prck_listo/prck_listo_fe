import { Component } from 'react'
import { ListGroupItem, Row, Col } from "react-bootstrap"


export default class List extends Component {
    //ciclo de vida
    //



    //acciones de formulario
    handleSelectList = () =>{
        this.props.selectAList(this.props.list)
    }
    //



    //funciones
    //



    //renderizado
    render() {
        const { list } = this.props
        return (
            <ListGroupItem action variant={list.color} onClick={this.handleSelectList}>
                <Row>
                    <Col>{list.nombre}</Col>
                    <Col>{list.cantidad}</Col>
                </Row>
            </ListGroupItem>
        )
    }
}