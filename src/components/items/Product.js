import React, {Component} from 'react'
import { Row,Col,ListGroupItem } from "react-bootstrap";

export default class Product extends Component{
    render(){
        const {product} = this.props
        return(
            <ListGroupItem>
                <Row>
                    <Col>
                        {product.nombre}
                    </Col>
                    <Col>
                        {product.cantidad}
                    </Col>
                    <Col>
                        {product.precioUnitario}
                    </Col>
                    <Col>
                        {product.total}
                    </Col>
                </Row>
            </ListGroupItem>
        )
    }
}