import { Component } from "react";
import { Card,Form} from "react-bootstrap"

export class Login extends Component{

    //ciclo de vida
    //



    //controladores de formulario
    handleSubmit=async e=>{
        e.preventDefault()
        this.access('/')
    }
    //



    //funciones locales
    access = (route) => {window.location.href=route}
    //
    
    
    //renderizado
    render(){
        return(
            <Card>
                <Card.Header>LOGIN</Card.Header>
                <Card.Body>
                    <Form className='my-4' onSubmit={this.handleSubmit}>
                        <Form.Group className='mt-2'>
                            <Form.Label>usuario</Form.Label>
                            <Form.Control type='text' required/>
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Label>contraseña</Form.Label>
                            <Form.Control type='password' required/>
                        </Form.Group>
                        <Form.Group className='mt-5'>
                            <Form.Control type='submit' value='acceder' className='btn btn-primary'/>
                        </Form.Group>
                    </Form>
                </Card.Body>
            </Card>
        )
    }
}