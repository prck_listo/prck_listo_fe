import { Component } from 'react'
import { ListGroup, Container, Row, Col } from "react-bootstrap";
import List from "../components/items/List"
const db = require('../example')


export default class ListView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            lists: [],
            listSelected:{}
        }
    }



    //ciclo de vida
    componentDidMount() {
        this.setState({ lists: db.listas })
    }
    //



    //acciones de formulario
    //



    //funciones
        selectAList=(list)=>{
            this.props.actualizarLista(list)
            this.setState({listSelected:list})
            window.location.href = `/list/${list.idLista}`
        }
    //



    //renderizado
    render() {
        return (
            <Container>
                <Row>
                    Listas de compra
                </Row>
                <Row>
                    <Col xl={{ span: 8, offset: 2 }} lg={{ span: 8, offset: 2 }}>
                        <ListGroup >
                            <ListGroup.Item className='bg-light'>
                                <Row>
                                    <Col><strong>nombre lista</strong></Col>
                                    <Col><strong>cantidad</strong></Col>
                                </Row>
                            </ListGroup.Item>
                            {this.state.lists
                                .sort((listPrev, listPost) => listPrev.linea - listPost.linea)
                                .map((list, indice) => (<List key={indice} list={list} selectAList={this.selectAList}/>))}
                        </ListGroup>
                    </Col>
                </Row>

            </Container>

        )
    }
}