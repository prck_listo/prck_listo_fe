import React, { Component } from 'react'
import { Container, Row, Col, ListGroup } from "react-bootstrap"
import Product from "../components/items/Product"
const db = require('../example.json')

export default class ProductsView extends Component {
    constructor(props){        
        super(props)
        
        this.state = {
            productos : []
        }
    }
    componentDidMount(){
        this.getProducts(this.props.match.params.id)
    }

    getProducts =async id =>{
        const productos = await db.productos.filter(producto => producto.idLista === id)
        this.setState({productos})
    }
    render() {
        return (
            <Container>
                <Row>PRODUCTOS</Row>
                <Row>
                    <Col xl={{ span: 8, offset: 2 }} lg={{ span: 8, offset: 2 }}>
                        <ListGroup>
                            <ListGroup.Item variant='light'>
                                <Row>
                                    <Col>producto</Col>
                                    <Col>cantidad</Col>
                                    <Col>precio</Col>
                                    <Col>total</Col>
                                </Row>
                            </ListGroup.Item>
                            {this.state.productos.map((product,indice) => (<Product key={indice} product={product}/>))}
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        )
    }
}