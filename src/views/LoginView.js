import { Component } from "react"
import { Container,Row,Col } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { Login } from "../components/Forms/Login"

export class LoginView extends Component{
    constructor(){
        super()
        this.state= {
            logged:true
        }
    }
    componentDidMount(){
        if (this.state.logged) {
            <Redirect to='/'/>
        }
    }
    render(){
        return(
            <Container>
                <Row>
                    <Col xl={{span:4,offset:4}}>
                        <Login/>
                    </Col>
                </Row>
            </Container>
        )
    }
}