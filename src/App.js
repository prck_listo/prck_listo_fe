import { Component } from "react";
import { LoginView } from "./views/LoginView"
import ListView from "./views/ListView"
import ProductsView from "./views/ProductsView"
import { BrowserRouter, Route, Switch } from "react-router-dom"

export class App extends Component {
  constructor() {
    super()
    this.state = {
      listaSeleccionada: {}
    }
  }


  actualizarLista = lista => {
    this.setState({ listaSeleccionada: lista })
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/'>
            <ListView actualizarLista={this.actualizarLista}/>
          </Route>

          <Route path='/list/:id' component={ProductsView}>
            {/* <ProductsView /> */}
          </Route>
          <Route exact path='/login' component={LoginView} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App