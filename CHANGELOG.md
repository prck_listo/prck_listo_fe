# LISTOAPP

## **versión 1.0.0**     12-07-2021 | ?

> instancia del proyecto inicial

### v1.1

> acceso a las distintas vistas

### sv.6

- acceso a parámetros de la url
- lista de productos según lista seleccionada

### sv.5

- acceso desde login a lista
- preventdefault y corrección de pestañéo
- acceso de listas a lista
- pendiente envío de datos para cargar según lista seleccionada

### sv.4

- vista detalle de lista (productos)
- ruta de acceso a detalle
- vista básica de detalle de lista

### sv.3

- vista listas
- componente item de lista
- datos de muestra

#### sv.2

- formulario de login
- eliminación de archivos por defecto
- inicios del redireccionamiento

#### sv.1

- limpieza de datos por defecto
- instalaciones básicas
